# Decrypto

This is a replica for Decrypto Android app, written in HTML, CSS and JavaScript.

Powered by ChatGPT.

## Missing algorithms

- Numeral code: Decrypto app provide a option to set shift code, but ChatGPT doesn't understand it.
- Rotation code: ChatGPT said it's no difference with Caesar code.
- Character encoding

## External libraries
- blowfish.js: for Blowfish code
- crypto-js.js: for MD5, SHA-1 and SHA-256
- jsencrypt.js: for RSA code

## License

All code are licensed under the Unlicense, except for libraries in js/external/ folder, they have their own copyright owners.

## Credits

Thanks [Maximus_ckp](https://web.archive.org/web/20231010144946/https://blog.csdn.net/Maximus_ckp/article/details/81168475) for JSEncrypt guide.
