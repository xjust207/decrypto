// 将二进制字符串转换回文本
function fromBinary(binaryString) {
    let text = '';
    for (let i = 0; i < binaryString.length; i += 8) {
        const byte = binaryString.slice(i, i + 8);
        text += String.fromCharCode(parseInt(byte, 2));
    }
    return text;
}

// 将ASCII码字符串转换回文本
function fromASCII(asciiString) {
    const asciiArray = asciiString.split(' ').map(Number);
    let text = '';
    for (let i = 0; i < asciiArray.length; i++) {
        const charCode = asciiArray[i];
        text += String.fromCharCode(charCode);
    }
    return text;
}

// 将Base64编码转换回文本
function fromBase64(base64Text) {
    const binaryString = atob(base64Text);
    const utf8Bytes = new Uint8Array(binaryString.length);
    
    for (let i = 0; i < binaryString.length; i++) {
        utf8Bytes[i] = binaryString.charCodeAt(i);
    }
    
    return new TextDecoder().decode(utf8Bytes);
}

// 将十六进制编码转换回文本
function fromHexadecimal(hexText) {
    const hexArray = hexText.match(/.{1,2}/g); // 将十六进制字符串拆分为每两个字符
    if (!hexArray) {
        throw new Error('Invalid hexadecimal input');
    }
    
    return hexArray.map(hexChar => String.fromCharCode(parseInt(hexChar, 16))).join('');
}

// 解密函数
function blowfishDecipher(ciphertext, key) {
    // 将密文转换为WordArray对象
    const ciphertextData = CryptoJS.enc.Base64.parse(ciphertext); // 使用Base64而不是Hex

    // 将密钥转换为WordArray对象
    const keyData = CryptoJS.enc.Utf8.parse(key);
    
    // 使用Blowfish算法进行解密
    const decrypted = CryptoJS.Blowfish.decrypt({
        ciphertext: ciphertextData
    }, keyData, {
        mode: CryptoJS.mode.ECB, // 使用ECB模式
        padding: CryptoJS.pad.Pkcs7 // 使用PKCS7填充
    });
    
    // 将解密结果转换为UTF-8字符串
    return decrypted.toString(CryptoJS.enc.Utf8);
}

// 解密函数
function playfairDecipher(text, key) {
    text = text.toUpperCase(); // 将文本转换为大写
    const matrix = createPlayfairMatrix(key);
    const pairs = prepareText(text);
    let result = "";

    for (let pair of pairs) {
        const char1 = pair[0];
        const char2 = pair[1];
        let row1, col1, row2, col2;

        for (let i = 0; i < matrix.length; i++) {
            if (matrix[i] === char1) {
                row1 = Math.floor(i / 5);
                col1 = i % 5;
            }
            if (matrix[i] === char2) {
                row2 = Math.floor(i / 5);
                col2 = i % 5;
            }
        }

        let newChar1, newChar2;

        if (row1 === row2) {
            newChar1 = matrix[row1 * 5 + (col1 + 4) % 5];
            newChar2 = matrix[row2 * 5 + (col2 + 4) % 5];
        } else if (col1 === col2) {
            newChar1 = matrix[((row1 + 4) % 5) * 5 + col1];
            newChar2 = matrix[((row2 + 4) % 5) * 5 + col2];
        } else {
            newChar1 = matrix[row1 * 5 + col2];
            newChar2 = matrix[row2 * 5 + col1];
        }

        result += newChar1 + newChar2;
    }

    return result;
}

// AES解密函数
function aesDecipher(ciphertext, key) {
    // 将密文和密钥转换为字节数组
    const ciphertextBytes = CryptoJS.enc.Base64.parse(ciphertext);
    const keyBytes = CryptoJS.enc.Utf8.parse(key);

    // 使用AES算法和密钥进行解密
    const decrypted = CryptoJS.AES.decrypt(
        { ciphertext: ciphertextBytes },
        keyBytes,
        {
            mode: CryptoJS.mode.ECB, // 使用ECB模式
            padding: CryptoJS.pad.Pkcs7 // 使用PKCS7填充
        }
    );

    // 将解密后的数据转换为文本
    const plaintext = CryptoJS.enc.Utf8.stringify(decrypted);

    return plaintext;
}

// Pigpen密码解密函数，包括小写字母
function pigpenDecipher(ciphertext) {
    const alphabet = {
        '⌷': 'A', '⌸': 'B', '⌹': 'C', '⌺': 'D', '⌻': 'E', '⌼': 'F',
        '⌽': 'G', '⌾': 'H', '⌿': 'I', '⍀': 'J', '⍁': 'K', '⍂': 'L',
        '⍃': 'M', '⍄': 'N', '⍅': 'O', '⍆': 'P', '⍇': 'Q', '⍈': 'R',
        '⍉': 'S', '⍊': 'T', '⍋': 'U', '⍌': 'V', '⍍': 'W', '⍎': 'X',
        '⍏': 'Y', '⍐': 'Z',
        'a': '⌷', 'b': '⌸', 'c': '⌹', 'd': '⌺', 'e': '⌻', 'f': '⌼',
        'g': '⌽', 'h': '⌾', 'i': '⌿', 'j': '⍀', 'k': '⍁', 'l': '⍂',
        'm': '⍃', 'n': '⍄', 'o': '⍅', 'p': '⍆', 'q': '⍇', 'r': '⍈',
        's': '⍉', 't': '⍊', 'u': '⍋', 'v': '⍌', 'w': '⍍', 'x': '⍎',
        'y': '⍏', 'z': '⍐',
        ' ': ' ', // 空格保持不变
    };

    let plaintext = '';
    for (let i = 0; i < ciphertext.length; i++) {
        const char = ciphertext[i];
        if (alphabet[char]) {
            plaintext += alphabet[char];
        } else {
            plaintext += char; // 如果字符不在字母表中，保持不变
        }
    }

    return plaintext;
}

// RSA解密函数
function rsaDecrypt(ciphertext, privateKey) {
    try {
        var decrypt = new JSEncrypt();
        decrypt.setPrivateKey(privateKey.value);
        var plaintext = decrypt.decrypt(ciphertext);
        return plaintext;
    } catch (e) {
        return "解密出错：" + e;
    }
}