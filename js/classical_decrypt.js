function caesarDecipher(text, shift) {
    // 解密即为加密的逆操作，所以我们将位移值取负
    return caesarCipher(text, -shift);
}

function romanDecipher(encryptedText) {
    // 创建逆映射表，将罗马数字映射回字母
    const reverseRomanMap = {
        'I': 'A',
        'II': 'B',
        'III': 'C',
        'IV': 'D',
        'V': 'E',
        'VI': 'F',
        'VII': 'G',
        'VIII': 'H',
        'IX': 'I',
        'X': 'J',
        'XI': 'K',
        'XII': 'L',
        'XIII': 'M',
        'XIV': 'N',
        'XV': 'O',
        'XVI': 'P',
        'XVII': 'Q',
        'XVIII': 'R',
        'XIX': 'S',
        'XX': 'T',
        'XXI': 'U',
        'XXII': 'V',
        'XXIII': 'W',
        'XXIV': 'X',
        'XXV': 'Y',
        'XXVI': 'Z',
    };

    // 按空格拆分加密文本，然后将每个罗马数字映射回字母
    const decryptedText = encryptedText.split(' ').map(roman => {
        if (reverseRomanMap.hasOwnProperty(roman)) {
            return reverseRomanMap[roman];
        } else {
            return roman; // 非罗马数字字符不变
        }
    }).join('');

    return decryptedText;
}

// 解密函数：与加密相同，将文本逆序排列即可解密
function reverseDecipher(text) {
    return text.split('').reverse().join('');
}

function morseDecipher(morseCode) {
    // 创建逆向的摩尔斯码映射表，将摩尔斯码字母映射回字母和数字
    const reverseMorseMap = {
        '.-': 'A', '-...': 'B', '-.-.': 'C', '-..': 'D', '.': 'E', '..-.': 'F', '--.': 'G', '....': 'H', '..': 'I', '.---': 'J',
        '-.-': 'K', '.-..': 'L', '--': 'M', '-.': 'N', '---': 'O', '.--.': 'P', '--.-': 'Q', '.-.': 'R', '...': 'S', '-': 'T',
        '..-': 'U', '...-': 'V', '.--': 'W', '-..-': 'X', '-.--': 'Y', '--..': 'Z',
        '-----': '0', '.----': '1', '..---': '2', '...--': '3', '....-': '4', '.....': '5', '-....': '6', '--...': '7', '---..': '8', '----.': '9',
        '/': ' ' // 空格
    };

    // 将摩尔斯码文本分割为单词
    const morseWords = morseCode.split(' / ');

    // 将每个单词的摩尔斯码转换为字母和数字
    const decryptedText = morseWords.map(morseWord => {
        const morseLetters = morseWord.split(' ');
        const decryptedWord = morseLetters.map(morseLetter => {
            if (reverseMorseMap.hasOwnProperty(morseLetter)) {
                return reverseMorseMap[morseLetter];
            } else {
                // 非摩尔斯码字母字符不变
                return morseLetter;
            }
        }).join('');
        return decryptedWord;
    }).join(' ');

    return decryptedText;
}


function vigenereDecipher(text, key) {
    if (!text || !key) {
        return "请输入文本和密钥。";
    }

    text = text.toUpperCase();
    key = key.toUpperCase();

    let result = "";
    let keyIndex = 0;

    for (let i = 0; i < text.length; i++) {
        const charCode = text.charCodeAt(i);

        if (charCode >= 65 && charCode <= 90) { // 检查是否为大写字母
            const shift = key.charCodeAt(keyIndex) - 65; // 获取密钥字母的偏移值

            const decryptedCharCode = ((charCode - 65 - shift + 26) % 26) + 65;
            result += String.fromCharCode(decryptedCharCode);

            keyIndex = (keyIndex + 1) % key.length; // 循环使用密钥中的字母
        } else {
            result += text.charAt(i); // 非字母字符保持不变
        }
    }

    return result;
}

function atbashDecipher(text) {
    // 将输入文本中的字母映射为亚特巴什密码的对应字母
    const decryptedText = text.split('').map(char => {
        const uppercaseChar = char.toUpperCase();
        if (atbashMap.hasOwnProperty(uppercaseChar)) {
            return atbashMap[uppercaseChar];
        } else {
            return char; // 非字母字符不变
        }
    }).join('');

    return decryptedText;
}

function affineDecipher(text, a, b) {
    // 创建字母表
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const result = [];

    // 计算 a 的模逆
    const aInverse = modInverse(a, 26);

    for (const element of text) {
        const char = element;
        const isUppercase = char === char.toUpperCase();

        if (alphabet.includes(char.toUpperCase())) {
            const charIndex = alphabet.indexOf(char.toUpperCase());
            let decryptedCharIndex = aInverse * (charIndex - b) % 26;

            if (decryptedCharIndex < 0) {
                decryptedCharIndex += 26;
            }

            const decryptedChar = isUppercase
                ? alphabet[decryptedCharIndex]
                : alphabet[decryptedCharIndex].toLowerCase();

            result.push(decryptedChar);
        } else {
            result.push(char); // 非字母字符不变
        }
    }

    return result.join('');
}

// 解密函数
function polybiusDecipher(text) {
    const square = createPolybiusSquare();
    const numbers = text.match(/\d{2}| /g);
    let result = "";

    for (const element of numbers) {
        const numberPair = element;
        if (numberPair === " ") {
            result += " "; // 空格保持不变
        } else {
            const row = parseInt(numberPair[0]) - 1;
            const col = parseInt(numberPair[1]) - 1;
            result += square[row][col];
        }
    }

    return result;
}

// Scytale解密函数
function scytaleDecipher(text, diameter) {
    const numRows = Math.ceil(text.length / diameter);
    const matrix = new Array(numRows);
    let index = 0;

    // 初始化矩阵
    for (let i = 0; i < numRows; i++) {
        matrix[i] = new Array(diameter);
    }

    // 将文本填充到矩阵中
    for (let row = 0; row < numRows; row++) {
        for (let col = 0; col < diameter; col++) {
            if (index < text.length) {
                matrix[row][col] = text[index];
                index++;
            } else {
                matrix[row][col] = ' '; // 填充空格
            }
        }
    }

    // 从矩阵中按列读取解密后的文本
    let result = '';
    for (let col = 0; col < diameter; col++) {
        for (let row = 0; row < numRows; row++) {
            result += matrix[row][col];
        }
    }

    return result.trim(); // 去除末尾的空格
}

// Railfence解密函数
function railfenceDecipher(text, rails) {
    if (rails < 2 || text.length < 2) {
        return text; // 不需要解密
    }

    const textLength = text.length;
    const fence = Array.from({ length: rails }, () => Array(textLength).fill('.'));
    let rail = 0;
    let direction = 1;

    for (let i = 0; i < textLength; i++) {
        fence[rail][i] = ' ';
        rail += direction;
        if (rail === 0 || rail === rails - 1) {
            direction *= -1;
        }
    }

    let charIndex = 0;
    for (let i = 0; i < rails; i++) {
        for (let j = 0; j < textLength; j++) {
            if (fence[i][j] === ' ' && charIndex < textLength) {
                fence[i][j] = text[charIndex++];
            }
        }
    }

    rail = 0;
    direction = 1;
    let result = "";

    for (let i = 0; i < textLength; i++) {
        result += fence[rail][i];
        rail += direction;
        if (rail === 0 || rail === rails - 1) {
            direction *= -1;
        }
    }

    return result;
}

// Templar解密函数
function templarDecipher(text) {
    const reversetemplarAlphabet = {
        '🜜': 'A', '🝅': 'B', '🜚': 'C', '🝃': 'D', '🜓': 'E', '🜗': 'F',
        '🜯': 'G', '🝬': 'H', '🝊': 'I', '🝮': 'J', '🝉': 'K', '🝛': 'L',
        '🜩': 'M', '🜲': 'N', '🜩': 'O', '🜚': 'P', '🜱': 'Q', '🜙': 'R',
        '🜆': 'S', '🜧': 'T', '🝄': 'U', '🝘': 'V', '🝁': 'W', '🝞': 'X',
        '🝕': 'Y', '🝤': 'Z',
        '🜜': 'a', '🝅': 'b', '🜚': 'c', '🝃': 'd', '🜓': 'e', '🜗': 'f',
        '🜯': 'g', '🝬': 'h', '🝊': 'i', '🝮': 'j', '🝉': 'k', '🝛': 'l',
        '🜩': 'm', '🜲': 'n', '🜩': 'o', '🜚': 'p', '🜱': 'q', '🜙': 'r',
        '🜆': 's', '🜧': 't', '🝄': 'u', '🝘': 'v', '🝁': 'w', '🝞': 'x',
        '🝕': 'y', '🝤': 'z',
        ' ': ' ',
    };

    let result = '';
    for (const element of text) {
        const char = element;
        if (char in reversetemplarAlphabet) {
            result += reversetemplarAlphabet[char];
        } else {
            result += char;
        }
    }
    return result;
}

// 将位次还原为英文字符
function alphabetDecipher(numbers) {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    const numArray = numbers.split(' ');
    let result = '';

    for (let i = 0; i < numArray.length; i++) {
        const num = parseInt(numArray[i]);
        if (!isNaN(num) && num >= 1 && num <= 26) {
            const char = alphabet[num - 1];
            result += char;
        }
    }

    return result;
}
