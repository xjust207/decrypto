function caesarCipher(text, shift) {
    if (shift < 0) {
        shift = (shift % 26) + 26;
    }

    let result = "";

    for (const element of text) {
        let char = element;

        if (char.match(/[a-zA-Z]/)) {
            const isUpperCase = char === char.toUpperCase();
            const alphabet = isUpperCase ? 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' : 'abcdefghijklmnopqrstuvwxyz';
            const index = alphabet.indexOf(char);

            if (index !== -1) {
                const newIndex = (index + shift) % 26;
                char = isUpperCase ? alphabet[newIndex] : alphabet[newIndex].toLowerCase();
            }
        }

        result += char;
    }

    return result;
}

function romanCipher(text) {
    // 创建映射表，将字母映射到罗马数字
    const romanMap = {
        'A': 'I',
        'B': 'II',
        'C': 'III',
        'D': 'IV',
        'E': 'V',
        'F': 'VI',
        'G': 'VII',
        'H': 'VIII',
        'I': 'IX',
        'J': 'X',
        'K': 'XI',
        'L': 'XII',
        'M': 'XIII',
        'N': 'XIV',
        'O': 'XV',
        'P': 'XVI',
        'Q': 'XVII',
        'R': 'XVIII',
        'S': 'XIX',
        'T': 'XX',
        'U': 'XXI',
        'V': 'XXII',
        'W': 'XXIII',
        'X': 'XXIV',
        'Y': 'XXV',
        'Z': 'XXVI',
    };

    // 将输入文本中的字母映射为罗马数字并用空格隔开
    const encryptedText = text.split('').map(char => {
        const uppercaseChar = char.toUpperCase();
        if (romanMap.hasOwnProperty(uppercaseChar)) {
            return romanMap[uppercaseChar] + ' ';
        } else {
            return char; // 非字母字符不变
        }
    }).join('');

    return encryptedText;
}

// 加密函数：将文本逆序排列
function reverseCipher(text) {
    return text.split('').reverse().join('');
}

function morseCipher(text) {
    // 定义摩尔斯码字母映射
    const morseMap = {
        'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....', 'I': '..', 'J': '.---',
        'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-',
        'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..',
        '0': '-----', '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....', '6': '-....', '7': '--...', '8': '---..', '9': '----.',
        ' ': '/'
    };

    // 将输入文本的字母转换为摩尔斯码
    const upperText = text.toUpperCase();
    let morseCode = '';

    for (const element of upperText) {
        const char = element;
        if (morseMap.hasOwnProperty(char)) {
            morseCode += morseMap[char] + ' '; // 用空格分隔摩尔斯码字母
        } else {
            // 非字母字符不变
            morseCode += char;
        }
    }

    return morseCode.trim(); // 去除末尾的空格
}


function vigenereCipher(text, key) {
    if (!text || !key) {
        return "请输入文本和密钥。";
    }

    text = text.toUpperCase();
    key = key.toUpperCase();

    let result = "";
    let keyIndex = 0;

    for (let i = 0; i < text.length; i++) {
        const charCode = text.charCodeAt(i);

        if (charCode >= 65 && charCode <= 90) { // 检查是否为大写字母
            const shift = key.charCodeAt(keyIndex) - 65; // 获取密钥字母的偏移值

            const encryptedCharCode = ((charCode - 65 + shift) % 26) + 65;
            result += String.fromCharCode(encryptedCharCode);

            keyIndex = (keyIndex + 1) % key.length; // 循环使用密钥中的字母
        } else {
            result += text.charAt(i); // 非字母字符保持不变
        }
    }

    return result;
}

function atbashCipher(text) {
    // 将输入文本中的字母映射为亚特巴什密码的对应字母
    const encryptedText = text.split('').map(char => {
        const uppercaseChar = char.toUpperCase();
        if (atbashMap.hasOwnProperty(uppercaseChar)) {
            return atbashMap[uppercaseChar];
        } else {
            return char; // 非字母字符不变
        }
    }).join('');

    return encryptedText;
}

// 辅助函数：计算 a 的模逆
function modInverse(a, m) {
    for (let x = 1; x < m; x++) {
        if ((a * x) % m === 1) {
            return x;
        }
    }
    return 1;
}

function affineCipher(text, a, b) {
    // 创建字母表
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const result = [];

    for (const element of text) {
        const char = element;
        const isUppercase = char === char.toUpperCase();

        if (alphabet.includes(char.toUpperCase())) {
            const charIndex = alphabet.indexOf(char.toUpperCase());
            let encryptedCharIndex = (a * charIndex + b) % 26;

            if (encryptedCharIndex < 0) {
                encryptedCharIndex += 26;
            }

            const encryptedChar = isUppercase
                ? alphabet[encryptedCharIndex]
                : alphabet[encryptedCharIndex].toLowerCase();

            result.push(encryptedChar);
        } else {
            result.push(char); // 非字母字符不变
        }
    }

    return result.join('');
}

// 创建Polybius Square映射表格
function createPolybiusSquare() {
    const alphabet = "ABCDEFGHIKLMNOPQRSTUVWXYZ"; // 去掉字母J
    const square = [];
    let index = 0;

    for (let i = 0; i < 5; i++) {
        const row = [];
        for (let j = 0; j < 5; j++) {
            row.push(alphabet[index]);
            index++;
        }
        square.push(row);
    }

    return square;
}

// 加密函数
function polybiusCipher(text) {
    const square = createPolybiusSquare();
    const upperText = text.toUpperCase();
    let result = "";

    for (const element of upperText) {
        const char = element;
        if (char === "J") {
            result += "24"; // I和J通常合并在一起
        } else if (char === " ") {
            result += " "; // 空格保持不变
        } else {
            for (let row = 0; row < 5; row++) {
                for (let col = 0; col < 5; col++) {
                    if (square[row][col] === char) {
                        result += `${row + 1}${col + 1}`;
                    }
                }
            }
        }
    }

    return result;
}

// Scytale加密函数
function scytaleCipher(text, diameter) {
    const numRows = Math.ceil(text.length / diameter);
    const matrix = new Array(numRows);
    let index = 0;

    // 初始化矩阵
    for (let i = 0; i < numRows; i++) {
        matrix[i] = new Array(diameter);
    }

    // 将文本填充到矩阵中
    for (let col = 0; col < diameter; col++) {
        for (let row = 0; row < numRows; row++) {
            if (index < text.length) {
                matrix[row][col] = text[index];
                index++;
            } else {
                matrix[row][col] = ' '; // 填充空格
            }
        }
    }

    // 从矩阵中按行读取加密后的文本
    let result = '';
    for (let row = 0; row < numRows; row++) {
        for (let col = 0; col < diameter; col++) {
            result += matrix[row][col];
        }
    }

    return result;
}
// Railfence加密函数
function railfenceCipher(text, rails) {
    if (rails < 2 || text.length < 2) {
        return text; // 不需要加密
    }

    const textLength = text.length;
    const fence = Array.from({ length: rails }, () => Array(textLength).fill('.'));
    let rail = 0;
    let direction = 1;

    for (let i = 0; i < textLength; i++) {
        fence[rail][i] = text[i];
        rail += direction;
        if (rail === 0 || rail === rails - 1) {
            direction *= -1;
        }
    }

    let result = "";
    for (let i = 0; i < rails; i++) {
        for (let j = 0; j < textLength; j++) {
            if (fence[i][j] !== '.') {
                result += fence[i][j];
            }
        }
    }

    return result;
}

// Templar加密函数
function templarCipher(text) {
    const templarAlphabet = {
        'A': '🜜', 'B': '🝅', 'C': '🜚', 'D': '🝃', 'E': '🜓', 'F': '🜗',
        'G': '🜯', 'H': '🝬', 'I': '🝊', 'J': '🝮', 'K': '🝉', 'L': '🝛',
        'M': '🜩', 'N': '🜲', 'O': '🜩', 'P': '🜚', 'Q': '🜱', 'R': '🜙',
        'S': '🜆', 'T': '🜧', 'U': '🝄', 'V': '🝘', 'W': '🝁', 'X': '🝞',
        'Y': '🝕', 'Z': '🝤',
        'a': '🜜', 'b': '🝅', 'c': '🜚', 'd': '🝃', 'e': '🜓', 'f': '🜗',
        'g': '🜯', 'h': '🝬', 'i': '🝊', 'j': '🝮', 'k': '🝉', 'l': '🝛',
        'm': '🜩', 'n': '🜲', 'o': '🜩', 'p': '🜚', 'q': '🜱', 'r': '🜙',
        's': '🜆', 't': '🜧', 'u': '🝄', 'v': '🝘', 'w': '🝁', 'x': '🝞',
        'y': '🝕', 'z': '🝤',
        ' ': ' ',
    };

    let result = '';
    for (const element of text) {
        const char = element;
        if (char in templarAlphabet) {
            result += templarAlphabet[char];
        } else {
            result += char;
        }
    }
    return result;
}

// 将英文字符转换为在字母表中的位次
function alphabetCipher(text) {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    text = text.toLowerCase();
    let result = '';

    for (let i = 0; i < text.length; i++) {
        const char = text[i];
        if (char >= 'a' && char <= 'z') {
            const position = char.charCodeAt(0) - 'a'.charCodeAt(0) + 1; // 计算位次
            result += position + ' ';
        }
    }

    return result.trim(); // 去除末尾多余的空格并返回
}
