// 将文本转换为二进制字符串
function toBinary(text) {
    let binaryString = '';
    for (let i = 0; i < text.length; i++) {
        const char = text.charCodeAt(i).toString(2);
        binaryString += '0'.repeat(8 - char.length) + char;
    }
    return binaryString;
}

// 将文本转换为ASCII码字符串
function toASCII(text) {
    let asciiString = '';
    for (let i = 0; i < text.length; i++) {
        const charCode = text.charCodeAt(i);
        asciiString += charCode + ' ';
    }
    return asciiString.trim(); // 去除末尾的空格
}

// 将文本转换为Base64编码
function toBase64(text) {
    const utf8Bytes = new TextEncoder().encode(text);
    return btoa(String.fromCharCode(...utf8Bytes));
}

// 将文本转换为十六进制编码
function toHexadecimal(text) {
    return Array.from(text).map(char => char.charCodeAt(0).toString(16)).join('');
}

function md5Function(text) {
    // 使用 CryptoJS 库中的 MD5 哈希函数
    const hash = CryptoJS.MD5(text);
    return hash.toString(CryptoJS.enc.Hex);
}

function sha1Function(text) {
    // 使用 CryptoJS 库中的 SHA-1 哈希函数
    const hash = CryptoJS.SHA1(text);
    return hash.toString(CryptoJS.enc.Hex);
}

function sha256Function(text) {
    // 使用 CryptoJS 库中的 SHA-256 哈希函数
    const hash = CryptoJS.SHA256(text);
    return hash.toString(CryptoJS.enc.Hex);
}

// 加密函数
function blowfishCipher(text, key) {
    // 将文本和密钥转换为WordArray对象
    const textData = CryptoJS.enc.Utf8.parse(text);
    const keyData = CryptoJS.enc.Utf8.parse(key);
    
    // 使用Blowfish算法进行加密
    const encrypted = CryptoJS.Blowfish.encrypt(textData, keyData, {
        mode: CryptoJS.mode.ECB, // 使用ECB模式
        padding: CryptoJS.pad.Pkcs7 // 使用PKCS7填充
    });
    
    // 将加密结果转换为十六进制字符串
    return encrypted.toString();
}

// 创建 Playfair 密码矩阵
function createPlayfairMatrix(key) {
    const alphabet = "ABCDEFGHIKLMNOPQRSTUVWXYZ"; // 从 A 到 Z，去掉 J
    key = key.toUpperCase().replace(/J/g, "I"); // 将密钥转换为大写并替换 J 为 I
    const keySet = new Set(key);
    const matrix = [];

    // 填充矩阵
    for (let char of key) {
        keySet.delete(char);
        matrix.push(char);
    }

    for (let char of alphabet) {
        if (!keySet.has(char)) {
            matrix.push(char);
        }
    }

    return matrix.join("");
}

// 将明文拆分成双字母对
function prepareText(text) {
    text = text.toUpperCase().replace(/J/g, "I"); // 将文本转换为大写并替换 J 为 I
    const pairs = [];
    let i = 0;

    while (i < text.length) {
        if (i === text.length - 1) {
            pairs.push(text[i] + "X");
            break;
        }

        if (text[i] === text[i + 1]) {
            pairs.push(text[i] + "X");
            i++;
        } else {
            pairs.push(text[i] + text[i + 1]);
            i += 2;
        }
    }

    return pairs;
}

// 加密函数
function playfairCipher(text, key) {
    text = text.toUpperCase(); // 将文本转换为大写
    const matrix = createPlayfairMatrix(key);
    const pairs = prepareText(text);
    let result = "";

    for (let pair of pairs) {
        const char1 = pair[0];
        const char2 = pair[1];
        let row1, col1, row2, col2;

        for (let i = 0; i < matrix.length; i++) {
            if (matrix[i] === char1) {
                row1 = Math.floor(i / 5);
                col1 = i % 5;
            }
            if (matrix[i] === char2) {
                row2 = Math.floor(i / 5);
                col2 = i % 5;
            }
        }

        let newChar1, newChar2;

        if (row1 === row2) {
            newChar1 = matrix[row1 * 5 + (col1 + 1) % 5];
            newChar2 = matrix[row2 * 5 + (col2 + 1) % 5];
        } else if (col1 === col2) {
            newChar1 = matrix[((row1 + 1) % 5) * 5 + col1];
            newChar2 = matrix[((row2 + 1) % 5) * 5 + col2];
        } else {
            newChar1 = matrix[row1 * 5 + col2];
            newChar2 = matrix[row2 * 5 + col1];
        }

        result += newChar1 + newChar2;
    }

    return result;
}

// AES加密函数
function aesCipher(text, key) {
    // 将文本和密钥转换为字节数组
    const textBytes = CryptoJS.enc.Utf8.parse(text);
    const keyBytes = CryptoJS.enc.Utf8.parse(key);

    // 使用AES算法和密钥进行加密
    const encrypted = CryptoJS.AES.encrypt(textBytes, keyBytes, {
        mode: CryptoJS.mode.ECB, // 使用ECB模式
        padding: CryptoJS.pad.Pkcs7 // 使用PKCS7填充
    });

    // 将加密后的数据转换为Base64字符串
    //const ciphertext = CryptoJS.enc.Base64.stringify(encrypted.ciphertext);

    //return ciphertext;
    return encrypted;
}

// 生成随机的 AES 密钥
function generateAESKey() {
    // 使用 CryptoJS 生成一个 256 位（32 字节）的随机密钥
    const randomKey = CryptoJS.lib.WordArray.random(32);

    // 将随机密钥转换为 Base64 编码的字符串
    const keyBase64 = CryptoJS.enc.Base64.stringify(randomKey);

    return keyBase64;
}

// Pigpen密码加密函数，包括小写字母
function pigpenCipher(plaintext) {
    const alphabet = {
        'A': '⌷', 'B': '⌸', 'C': '⌹', 'D': '⌺', 'E': '⌻', 'F': '⌼',
        'G': '⌽', 'H': '⌾', 'I': '⌿', 'J': '⍀', 'K': '⍁', 'L': '⍂',
        'M': '⍃', 'N': '⍄', 'O': '⍅', 'P': '⍆', 'Q': '⍇', 'R': '⍈',
        'S': '⍉', 'T': '⍊', 'U': '⍋', 'V': '⍌', 'W': '⍍', 'X': '⍎',
        'Y': '⍏', 'Z': '⍐',
        'a': '⌷', 'b': '⌸', 'c': '⌹', 'd': '⌺', 'e': '⌻', 'f': '⌼',
        'g': '⌽', 'h': '⌾', 'i': '⌿', 'j': '⍀', 'k': '⍁', 'l': '⍂',
        'm': '⍃', 'n': '⍄', 'o': '⍅', 'p': '⍆', 'q': '⍇', 'r': '⍈',
        's': '⍉', 't': '⍊', 'u': '⍋', 'v': '⍌', 'w': '⍍', 'x': '⍎',
        'y': '⍏', 'z': '⍐',
        ' ': ' ', // 空格保持不变
    };

    let ciphertext = '';
    for (let i = 0; i < plaintext.length; i++) {
        const char = plaintext[i];
        if (alphabet[char]) {
            ciphertext += alphabet[char];
        } else {
            ciphertext += char; // 如果字符不在字母表中，保持不变
        }
    }

    return ciphertext;
}

function generateRSAKeyPair(keyLength) {
    const encrypt = new JSEncrypt();
    encrypt.getKey(keyLength);
    return {
        publicKey: encrypt.getPublicKey(),
        privateKey: encrypt.getPrivateKey()
    };
}

// RSA加密函数
function rsaEncrypt(text, publicKey) {
    try {
        var encrypt = new JSEncrypt();
        encrypt.setPublicKey(publicKey.value);
        var ciphertext = encrypt.encrypt(text);
        return ciphertext;
    } catch (e) {
        return "加密出错：" + e;
    }
}