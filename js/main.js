document.addEventListener("DOMContentLoaded", function () {
    // 根据用户选择的操作和算法，显示/隐藏密钥输入框和生成按钮
    document.getElementById("algorithm").addEventListener("change", function () {
        const algorithm = document.getElementById("algorithm").value;
        const keyInput = document.getElementById("keyInput");
        const generateAESKeyButton = document.getElementById("generateAESKeyButton");
        const diameterInput = document.getElementById("diameterInput");
        const railInput = document.getElementById("railInput");
        const shiftInput = document.getElementById("shiftInput");
        const affineInput = document.getElementById("affineInput");
        const rsaOptions = document.getElementById("rsaOptions");
        if (algorithm === "aes") {
            generateAESKeyButton.style.display = "block";
        } else {
            generateAESKeyButton.style.display = "none";
        }
        if (algorithm === "caesar") {
            shiftInput.style.display = "block";
        } else {
            shiftInput.style.display = "none";
        }
        if (algorithm === "affine") {
            affineInput.style.display = "block";
        } else {
            affineInput.style.display = "none";
        }
        if (
            algorithm === "vigenere" ||
            algorithm === "playfair" ||
            algorithm === "blowfish" ||
            algorithm === "aes"
        ) {
            keyInput.style.display = "block";
        } else {
            keyInput.style.display = "none";
        }
        if (algorithm === "scytale") {
            diameterInput.style.display = "block";
        } else {
            diameterInput.style.display = "none";
        }
        if (algorithm === "railfence") {
            railInput.style.display = "block";
        } else {
            railInput.style.display = "none";
        }
        if (algorithm === "rsa") {
            rsaOptions.style.display = "block";
        } else {
            rsaOptions.style.display = "none";
        }
    });
    document.getElementById("operation").addEventListener("change", function () {
        const operation = document.getElementById("operation").value;
        const algorithm = document.getElementById("algorithm").value;
        const keyInput = document.getElementById("keyInput");
        const diameterInput = document.getElementById("diameterInput");
        const railInput = document.getElementById("railInput");
        const shiftInput = document.getElementById("shiftInput");
        const affineInput = document.getElementById("affineInput");
        // 根据用户选择的算法，显示/隐藏密钥输入框
        if ((operation === "encrypt" && algorithm === "vigenere") ||
            (operation === "decrypt" && algorithm === "vigenere") ||
            (operation === "encrypt" && algorithm === "blowfish") ||
            (operation === "decrypt" && algorithm === "blowfish") ||
            (operation === "encrypt" && algorithm === "playfair") ||
            (operation === "decrypt" && algorithm === "playfair") ||
            (operation === "encrypt" && algorithm === "aes") ||
            (operation === "decrypt" && algorithm === "aes")) {
            keyInput.style.display = "block";
        } else {
            keyInput.style.display = "none";
        }
        // 根据用户选择的算法，显示/隐藏diameter输入框
        if ((operation === "encrypt" && algorithm === "scytale") ||
            (operation === "decrypt" && algorithm === "scytale")) {
            diameterInput.style.display = "block";
        } else {
            diameterInput.style.display = "none";
        }
        // 根据用户选择的算法，显示/隐藏rail输入框
        if ((operation === "encrypt" && algorithm === "railfence") ||
            (operation === "decrypt" && algorithm === "railfence")) {
            railInput.style.display = "block";
        } else {
            railInput.style.display = "none";
        }
        // 在用户选择加密/解密算法时，显示/隐藏位移输入框
        if ((operation === "encrypt" && algorithm === "caesar") ||
            (operation === "decrypt" && algorithm === "caesar")) {
            shiftInput.style.display = "block";
        } else {
            shiftInput.style.display = "none";
        }
        // 根据用户选择的算法，显示/隐藏a, b输入框
        if ((operation === "encrypt" && algorithm === "affine") ||
            (operation === "decrypt" && algorithm === "affine")) {
            affineInput.style.display = "block";
        } else {
            affineInput.style.display = "none";
        }
    });
    // 添加生成 AES 密钥的逻辑
    document.getElementById("generateKeyButton").addEventListener("click", function () {
        let generatedAESKey = generateAESKey(); // 生成 AES 密钥并存储在全局变量中
        document.getElementById("key").value = generatedAESKey; // 将生成的密钥填充到输入框中
    });
    // 添加生成 RSA 密钥的逻辑
    document.getElementById("generateRSAKeyPairButton").addEventListener("click", function () {
        const keyLength = parseInt(document.getElementById("keyLength").value);
        const rsaKeyPair = generateRSAKeyPair(keyLength);
        // 显示公钥和私钥
        document.getElementById("publicKey").value = rsaKeyPair.publicKey;
        document.getElementById("privateKey").value = rsaKeyPair.privateKey;
    });
    // 处理加密/解密操作
    document.getElementById("performButton").addEventListener("click", function () {
        const inputText = document.getElementById("inputText").value;
        const operation = document.getElementById("operation").value;
        const algorithm = document.getElementById("algorithm").value;
        const key = document.getElementById("key").value; // 获取密钥
        let result = "";
        // 根据用户选择的操作和算法，执行相应的加密/解密操作
        if (operation === "encrypt") {
            switch (algorithm) {
                case "caesar":
                    const shift = parseInt(document.getElementById("shift").value);
                    result = caesarCipher(inputText, shift);
                    break;
                case "roman":
                    result = romanCipher(inputText);
                    break;
                case "reverse":
                    result = reverseCipher(inputText);
                    break;
                case "morse":
                    result = morseCipher(inputText);
                    break;
                case "vigenere":
                    result = vigenereCipher(inputText, key);
                    break;
                case "alphabet":
                    result = alphabetCipher(inputText);
                    break;
                case "atbash":
                    result = atbashCipher(inputText);
                    break;
                case "affine":
                    const a = parseInt(document.getElementById("a").value);
                    const b = parseInt(document.getElementById("b").value);
                    result = affineCipher(inputText, a, b);
                    break;
                case "polybius":
                    result = polybiusCipher(inputText);
                    break;
                case "scytale":
                    result = scytaleCipher(inputText, 3);
                    break;
                case "railfence":
                    const rails = parseInt(document.getElementById("rail").value);
                    result = railfenceCipher(inputText, rails);
                    break;
                case "pigpen":
                    result = pigpenCipher(inputText);
                    break;
                case "templar":
                    result = templarCipher(inputText);
                    break;
                case "binary":
                    result = toBinary(inputText);
                    break;
                case "ascii":
                    result = toASCII(inputText);
                    break;
                case "base64":
                    result = toBase64(inputText);
                    break;
                case "hexadecimal":
                    result = toHexadecimal(inputText);
                    break;
                case "md5":
                    result = md5Function(inputText);
                    break;
                case "sha1":
                    result = sha1Function(inputText);
                    break;
                case "sha256":
                    result = sha256Function(inputText);
                    break;
                case "blowfish":
                    result = blowfishCipher(inputText, key);
                    break;
                case "playfair":
                    result = playfairCipher(inputText, key);
                    break;
                case "aes":
                    result = aesCipher(inputText, key);
                    break;
                case "rsa":
                    result = rsaEncrypt(inputText, publicKey);
                    break;
                default:
                    alert("请选择一个加密方式。");
                    break;
            }
        } else if (operation === "decrypt") {
            switch (algorithm) {
                case "caesar":
                    const shift = parseInt(document.getElementById("shift").value);
                    result = caesarDecipher(inputText, shift);
                    break;
                case "roman":
                    result = romanDecipher(inputText);
                    break;
                case "reverse":
                    result = reverseDecipher(inputText);
                    break;
                case "morse":
                    result = morseDecipher(inputText);
                    break;
                case "vigenere":
                    result = vigenereDecipher(inputText, key);
                    break;
                case "alphabet":
                    result = alphabetDecipher(inputText);
                    break;
                case "atbash":
                    result = atbashDecipher(inputText);
                    break;
                case "affine":
                    const a = parseInt(document.getElementById("a").value);
                    const b = parseInt(document.getElementById("b").value);
                    result = affineDecipher(inputText, a, b);
                    break;
                case "polybius":
                    result = polybiusDecipher(inputText);
                    break;
                case "scytale":
                    result = scytaleDecipher(inputText, 3);
                    break;
                case "railfence":
                    const rails = parseInt(document.getElementById("rail").value);
                    result = railfenceDecipher(inputText, rails);
                    break;
                case "pigpen":
                    result = pigpenDecipher(inputText);
                    break;
                case "templar":
                    result = templarDecipher(inputText);
                    break;
                case "binary":
                    result = fromBinary(inputText);
                    break;
                case "ascii":
                    result = fromASCII(inputText);
                    break;
                case "base64":
                    result = fromBase64(inputText);
                    break;
                case "hexadecimal":
                    result = fromHexadecimal(inputText);
                    break;
                case "md5":
                case "sha1":
                case "sha256":
                    alert("Not possible to decrypt hash");
                    break;
                case "blowfish":
                    result = blowfishDecipher(inputText, key);
                    break;
                case "playfair":
                    result = playfairDecipher(inputText, key);
                    break;
                case "aes":
                    result = aesDecipher(inputText, key);
                    break;
                case "rsa":
                    result = rsaDecrypt(inputText, privateKey);
                    break;
                default:
                    alert("请选择一个解密方式。");
                    break;
            }
        }
        document.getElementById("output").innerText = result;
    });
});

// 为亚特巴什密码创建字母表的逆向映射表
const atbashMap = {
    'A': 'Z',
    'B': 'Y',
    'C': 'X',
    'D': 'W',
    'E': 'V',
    'F': 'U',
    'G': 'T',
    'H': 'S',
    'I': 'R',
    'J': 'Q',
    'K': 'P',
    'L': 'O',
    'M': 'N',
    'N': 'M',
    'O': 'L',
    'P': 'K',
    'Q': 'J',
    'R': 'I',
    'S': 'H',
    'T': 'G',
    'U': 'F',
    'V': 'E',
    'W': 'D',
    'X': 'C',
    'Y': 'B',
    'Z': 'A',
};